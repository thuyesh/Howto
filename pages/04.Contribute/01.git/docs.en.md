---
title: How-to Contribute: Git
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - git
page-toc:
    active: false
---


![](en/git.png)

# What is git?

**Git** is a distributed version control system, a tool for tracking files, code and content. It allows many people to work on the codes and follow all the changes at the same time with a copy of the project in all the developer's computers. It's very popular among developers and system administrators but it's features can be easily applicable everywhere history of changes and ability to submit content and collaborate in a group is required.

We use it as the main tool for the development of our howtos and website. And it's the one we prefer, mainly because its usage is quite simple, fast and it's very powerful. In addition, we also use **Atom**, a rich text and code editor, although you can use the text editor of your choice.

In the next pages we'll see how to use them for **Disroot**'s documentation.

### [Git: Basics How-to](how-to-use-git)

----
You can find more information about git [here](https://en.wikipedia.org/wiki/Git) and in [this article](https://medium.freecodecamp.org/what-is-git-and-how-to-use-it-c341b049ae61?gi=805863b5a598).
