---
title: How-to Contribute: Forum
published: false
visible: true
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - forum
page-toc:
    active: false
---


# Open Task

This is a good example for the work still to do. Until now, no one was found to write this Tutorial. Maybe you want to make it your first project?
