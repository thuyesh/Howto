---
title: Como contribuir: Forum
published: false
visible: true
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - forum
page-toc:
    active: false
---


# Por fazer

Isto é um bom exemplo do trabalho que ainda falta fazer. Até agora ainda não se encontrou ninguém para escrever este documento.<br>
Talvez escrever a documentação para esta página possa ser o teu primeiro projeto? :)
