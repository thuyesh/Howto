---
title: Contribuer
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Cette section est dédiée à aider toutes ces personnes impressionnantes qui ont décidé de nous aider avec des contributions aux traductions et à la création de tutoriels.
Ici, nous mettrons des informations de base et des lignes directrices pour rendre cette collaboration aussi douce et indolore que possible.


![](contribute.png)
