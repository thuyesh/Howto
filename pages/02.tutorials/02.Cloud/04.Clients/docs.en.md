---
title: Cloud: Clients
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - clients
visible: true
page-toc:
    active: false
---

<br>

### [Desktop clients and Integration](desktop)
- Nextcloud and others sync clients, settings

### [Mobile clients](mobile)
- Nextcloud Apps, clients, DAVx⁵, device settings
