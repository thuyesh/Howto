---
title: Mobile clients: Android
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - android
        - nextcloud
visible: true
page-toc:
     active: false
---

# Android Nextcloud Integration

The following How-tos will help you integrate **Nextcloud** with your **Android** device.

### [Nextcloud app](nextcloud-app)
### [Syncing Calendars, Contacts and Tasks](calendars-contacts-and-tasks)
### [Migrating contacts from Google into Nextcloud](migrating-contacts-from-google)
### [Syncing News](using-news)
### [Syncing Notes](using-notes)

![](android.jpg)
