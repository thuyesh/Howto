---
title: iOS
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - sync
        - iOS
visible: false
page-toc:
     active: false
---

# iOS Nextcloud Integration

Below you can learn how to integrate Nextcloud with your iOS device
### [Syncing Calendars](calendar-syncing)
### [Syncing Contacts](contact-syncing)

![](ios.png)
