---
title: 'Cloud: Mobile Clients'
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - nextcloud
indexed: true
visible: true
page-toc:
     active: false
---
## Syncing your devices

**Nextcloud** integrates with your device very easily, providing native experience for most devices and operating systems.

### [Android](android)
- Nextcloud apps

### [iOS](ios)
- iOS app
