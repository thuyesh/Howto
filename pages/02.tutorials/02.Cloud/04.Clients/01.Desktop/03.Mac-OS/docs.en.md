---
title: MacOS
visible: false
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - integration
        - macos
page-toc:
     active: false
---

# Syncing your data with MacOS

Below you can learn how to integrate Nextcloud with your MacOS device
### [Syncing Calendars](calendar-syncing)
### [Syncing Contacts](contact-syncing)

![](macos.jpg)
