---
title: Desktop integration: GNU/Linux
published: true
updated:
taxonomy:
    category:
            - docs
    tags:
            - cloud
            - integration
            - sync
            - linux
            - desktop
visible: false
page-toc:
     active: false
---

<br>

![](gnu_linux.png)

**Nextcloud** integrates exceedingly well with **GNU/Linux**. The how-tos below contains the information to set up the desktop integration.

### [GNOME](gnome-desktop-integration) Desktop Integration

### [KDE](kde-desktop-integration) Desktop Integration
