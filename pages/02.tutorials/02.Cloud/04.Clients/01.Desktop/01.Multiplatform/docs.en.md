---
title: Desktop Clients: Multiplatform
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - sync
page-toc:
    active: false
---

# Multiplatform Clients


### [Nextcloud client](desktop-sync-client)
- Desktop sync client

### [Thunderbird](thunderbird-calendar-contacts)
- Calendar, Contacts & Tasks sync

### [calcurse](calcurse-caldav)
- Calendar sync for the command line
