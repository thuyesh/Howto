---
title: "Nube: Clientes de Escritorio"
published: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - nube
        - integración
visible: true
page-toc:
    active: false
---

<br>

### [Clientes de Escritorio Multiplataforma](multiplatform)
- Clientes multiplataforma para **Nextcloud**

### [GNU/Linux](gnu-linux)
- Integración con el escritorio

### [MacOS](mac-os)
- Integración de dispositivos **MacOS**
