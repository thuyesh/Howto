---
title: Bookmarks: Desktop
published: true
visible: false
updated:
        last_modified: "July 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - bookmarks
page-toc:
    active: false
---

## Bookmarks
