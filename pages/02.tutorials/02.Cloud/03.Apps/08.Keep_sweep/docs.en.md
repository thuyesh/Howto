---
title: Cloud Apps: Keep or Sweep
published: true
visible: false
indexed: true
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - delete
visible: true
page-toc:
    active: false
---

## Keep or Sweep
The purpose of this app is to help you clean wasted or unused files from your data. It ramdomly displays elements of your data at a time and you decide whether to ✅ keep or ❌ sweep it.
