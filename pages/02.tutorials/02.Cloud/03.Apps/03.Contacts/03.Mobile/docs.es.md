---
title: "Contactos: Móviles"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - nube
        - contactos
        - móviles
page-toc:
    active: false
---

# Integración de contactos con el móvil

**Disroot** tiene la aplicación **Contactos** habilitada.

Para configurar y utilizar tus contactos de **Disroot** en tu dispositivo, lee el siguiente tutorial:

### [Nexctcloud: Clientes para móviles](tutorials/cloud/clients/mobile)
