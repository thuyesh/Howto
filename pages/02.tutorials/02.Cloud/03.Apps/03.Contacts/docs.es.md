---
title: "Nube - Apps: Contactos"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - nube
        - contactos
    visible: true
page-toc:
    active: false
---

## Contactos

### [Interfaz web](web)
- Crear, editar y sincronizar contactos

### [Clientes de Escritorio](desktop)
- Clientes de escritorio y configuraciones de integración para organizar y sincronizar tus contactos

### [Clientes de Móvil](/tutorials/cloud/clients/mobile)
- Clientes para móviles y configuraciones para organizar y sincronizar tus contactos
