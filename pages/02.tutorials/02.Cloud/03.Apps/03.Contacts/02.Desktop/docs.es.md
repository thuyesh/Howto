---
title: "Contactos: Escritorio"
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - nube
        - contactos
page-toc:
    active: false
---

# Integración con el escritorio de los Contactos

Puedes leer los siguientes tutoriales para tener tus **Contactos** sincronizados utilizando clientes de escritorio multiplataforma.

- [Thunderbird: Calendario / Contactos / Sincronización de Tareas](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

Alternativamente, puedes configurar y utilizar la integración con el escritorio.

- [GNOME: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
