---
title: Tasks: Web
published: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

## Tasks on web-interface

The Tasks app allows you to add and delete tasks. A task could be a meeting date reminder, a job that needs to be done, a personal or a group activity and a lot of other things.<br> In this howto we'll see how to create, edit and share tasks.

![](en/main.png)

#### Adding a new task
Click in the item box and enter the name of the new task.

![](en/add_task_box.png)

Once you did it, a new section will show at the right.

![](en/add_tasks.png)

Here you can set:

- **The start date and due date**

![](en/add_tasks_date_01.png) ![](en/add_tasks_date_02.png)
By clicking on the Start/Due option you can set the day and the hour of it. You can also set the task as an all day activity


- **The priority of the task**

![](en/add_tasks_priority.png)
By moving the bar, you can set the priority level from 1 to 9.

- **Set and edit the progress level of the task**

![](en/task_completed.png)
By moving the bar, you can set the task progress from 0% to 100%.

- **Tags**
