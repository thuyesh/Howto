---
title: Attività: Web
published: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

## Attività nell'interfaccia web

L'app Attività ti consente di aggiungere ed eliminare attività. Un'attività potrebbe essere un promemoria per la data di una riunione, un lavoro che deve essere svolto, un'attività personale o di gruppo e molte altre cose. 
<br> In questa guida vedremo come creare, modificare e condividere attività.

![](en/main.png)

#### Aggiungere una nuova attività
Fare clic nella casella dell'elemento e inserire il nome della nuova attività.

![](en/add_task_box.png)
Una volta fatto, una nuova sezione sarà mostrata sulla destra.

![](en/add_tasks.png)
Qui puoi definire:

- **La data di inizio e la data di fine**

![](en/add_tasks_date_01.png) ![](en/add_tasks_date_02.png)
Facendo clic sull'opzione Start è possibile impostare il giorno e l'ora. È inoltre possibile impostare l'attività come attività per tutto il giorno

- **Le priorità delle attività**

![](en/add_tasks_priority.png)
Spostando la barra, è possibile impostare il livello di priorità da 1 a 9.


- **Imposta e modifica il livello di avanzamento dell'attività**

![](en/task_completed.png)
Spostando la barra, è possibile impostare l'avanzamento dell'attività dallo 0% al 100%.

- **Tag**
