---
title: "Cloud App: Attività"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - task
        - sync
    visible: true
page-toc:
    active: false
---

# Tasks

### [Interfaccia web](web)
- Creare e configurare attività

### [Client desktop](desktop)
- Client desktop e applicazioni per l'organizzazione e la sincronizzazione di attività

### [Client mobile](mobile)
- Client mobile e impostazioni per l'organizzazione e la sincronizzazione delle attività
