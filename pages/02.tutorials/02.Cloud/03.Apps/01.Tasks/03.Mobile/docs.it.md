---
title: Tasks: dispositivi mobili
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - mobile
page-toc:
    active: false
---

## Integrazione delle attività nei dispositivi mobili

Per configurare e sincronizzare le **Attività** tramite un client mobile, consultare il tutorial seguente:

### Android
- [DAVx⁵ / OpenTasks](/tutorials/cloud/clients/mobile/android/calendars-contacts-and-tasks)
- [Nextcloud mobile app](/tutorials/cloud/clients/mobile/android/nextcloud-app)
