---
title: News: Mobile
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - news
page-toc:
    active: false
---

# Android
## [Nextcloud News app](android/nc_news)
