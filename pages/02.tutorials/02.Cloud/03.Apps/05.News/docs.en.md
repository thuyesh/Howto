---
title: Cloud Apps: News
published: true
visible: false
updated:
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - news
visible: true
page-toc:
    active: false
---

## News

### [Web interface](web)
- Managing and configuring news feeds

### Desktop clients (coming soon)
- News feeds desktop clients

### Mobile clients (coming soon)
- News Apps, device settings
