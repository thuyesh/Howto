---
title: "Nube - Apps: Noticias"
published: true
visible: false
updated:
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - news
visible: true
page-toc:
    active: false
---

## Noticias

### [Interfaz web](web)
- Gestionar y configurar fuentes de noticias

### Clientes de Escritorio (pronto)
- Clientes para escritorio de la aplicación Noticias

### Clientes para Móvil (pronto)
- Aplicación Noticias, configuraciones de dispositivos
