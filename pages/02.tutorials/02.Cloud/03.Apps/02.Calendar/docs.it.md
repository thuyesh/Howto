---
title: "Cloud App: Calendario"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - calendar
page-toc:
    active: false
---

## Calendario

### [Interfaccia web](web)
- Creare e configurare i calendari

### [Client desktop](desktop)
- Impostazioni di integrazione per l'organizzazione e la sincronizzazione dei calendari nei client desktop

### [Client mobile](/tutorials/cloud/clients/mobile)
- Impostazioni di integrazione per l'organizzazione e la sincronizzazione dei calendari nei client mobile
