---
title: "Cloud Apps: Calendar"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - calendar
page-toc:
    active: false
---

## Calendar

### [Web interface](web)
- Creating and configuring calendars

### [Desktop clients](desktop)
- Desktop clients and integration settings for organizing and synchronizing calendars

### [Mobile clients](/tutorials/cloud/clients/mobile)
- Mobile clients and settings for organizing and synchronizing calendars
