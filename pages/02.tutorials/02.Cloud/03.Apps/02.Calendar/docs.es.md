---
title: "Nube - Apps: Calendario"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - nube
        - calendario
page-toc:
    active: false
---

## Calendario

### [Interfaz web](web)
- Crear y configurar calendarios

### [Clientes de Escritorio](desktop)
- Clientes de escritorio y configuraciones de integración para organizar y sincronizar calendarios

### [Clientes de Móvil](/tutorials/cloud/clients/mobile)
- Clientes para móviles y configuraciones para organizar y sincronizar calendarios
