---
title: Calendario: Desktop
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Calendario: integrazione nel desktop
Puoi leggere le istruzione seguenti per sincronizzare i tuoi **calendari** utilizzando un client desktop multipiattaforma.

- [Thunderbird: Calendario / Contatti / Tasks sync](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)
- [calcurse: sincronizzazione calendario](/tutorials/cloud/clients/desktop/multiplatform/calcurse-caldav)

In alternativa, è possibile utilizzare e configurare l'integrazione desktop.

- [Integrazione nel desktop GNOME](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE: Integrazione nel desktop KDE](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
