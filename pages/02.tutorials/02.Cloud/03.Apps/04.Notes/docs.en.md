---
title: Cloud Apps: Notes
published: true
visible: false
updated:
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
visible: true
page-toc:
    active: false
---
## Notes

### [Web interface](web)
- Creating and editing notes

### [Desktop clients](desktop)
- Notes app desktop clients

### [Mobile clients](mobile)
- Notes App, DAVx⁵, device settings
