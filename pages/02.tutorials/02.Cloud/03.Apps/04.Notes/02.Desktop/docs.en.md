---
title: Notes: Desktop clients
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
        - desktop
page-toc:
    active: false
---
## Desktop clients

To use **Notes** (create, edit and share them), please, check the following tutorials:


#### [Nextcloud app](/tutorials/cloud/clients/desktop/multiplatform/desktop-sync-client)

#### [Tasks sync from Thunderbird](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts#tasks-integration-with-with-thunderbird)

----
### Related How-Tos

- [GNOME: Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE: Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
