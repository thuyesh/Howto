---
title: 'Forum'
subtitle: "Discourse Basics"
icon: fa-stack-exchange
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - Forum
        - Discourse
page-toc:
    active: false
---

![](/home/icons/discourse.png)

**Discourse** ist eine Open Source Software für Internet Foren und das Management von Mailinglisten.
<br>
<br>
