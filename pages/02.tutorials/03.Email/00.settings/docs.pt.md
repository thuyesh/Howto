---
title: Como configurar um cliente de email
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - settings
page-toc:
    active: false
---

Esta seção é dedicada a tentar convence-lo a deixar de usar um email a partir de um browser de Internet para uma experiência verdeiramente produtiva através de um cliente de email no seu dispositivo (desktop, portátil, telemóvel).

Se o seu cliente de email favorito não aparece listado nesta seção, siga as configurações geriais do servidor para ter tudo a funcionar. Configurar um cliente de email é bastante fácil (excepto se estiver a usar o outlook, mmas quem é que quer saber acerca disso :)

```
IMAP: disroot.org | SSL Port 993 | Authentication: Normal Password
SMTP: disroot.org | STARTTLS Port 587 | Authentication: Normal Password
SMTPS: disroot.org | TLS Port 465 | Authentication: Normal Password
POP: disroot.org | SSL Port 995 | Authentication: Normal Password
```
Se gostaria de ver um toturial sobre como configurar um cliente de email de que gosta, mas que ainda não está listado aqui, considere a possibilidade de escrever um toturial curto e partilhá-lo com a comunidadde.
