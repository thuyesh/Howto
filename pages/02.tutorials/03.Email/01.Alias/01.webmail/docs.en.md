---
title: Email Alias: Setup on webmail
published: true
visible: false
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
        - webmail
page-toc:
    active: true
---

# Setup Alias on Webmail
First of, login to your webmail and go to your mail settings (bottom left icon)

![](en/settings1.png)

When in Settings, go to **"Identities"** tab, click "**Add an Identity"** and fill in the form. Once done, hit **"Add"** button.

*(Every* **Disroot** *user has an* username@disr.it *alias to use by default)*

![](en/identity_add.gif)

# Set default
You can manage default identity, by simply dragging the identity to the top of the list.

![](en/identity_default.gif)

# Send email
To send email with your new alias, just click on the **"From"** field and select alias you want to use from the dropdown menu, when composing your mail.

![](en/identity_send.gif)
