---
title: 'Email: Desktop-Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

## Multiplattform-Mailclients
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)



## GNU/Linux: Email per Desktop-Integration
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)


![](de/email_icon.png)
