---
title: 'Email: Desktop Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

## Multiplatform Mail Clients
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)



## GNU/Linux: Email desktop integration
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)


![](en/email_icon.png)
