---
title: Cliente de email do iOS
published: true
visible: false
taxonomy:
    category:
        - docs
---

1. Abra as configurações do seu dispositivo iOS e vá a 'Mail, Contacts, Calendars'. Depois selecione 'Add Account'.

![](en/ios_mail1.PNG)

2. Escolha 'Other'.

![](en/ios_mail2.PNG)

3. E escolha 'Add Mail Account'.

![](en/ios_mail3.PNG)

4. Insera as suas credenciais e carregue em 'Next'.

![](en/ios_mail4.PNG)

5. Altere o hostname para disroot.org, tanto para o servidor de receção (incoming) como o servidor de envio de emails (outgoing).

![](en/ios_mail5.PNG)

Carregue em 'Next' e a sua conta deve estar pronta para ser usada com o seu cliente de email do IOS.
