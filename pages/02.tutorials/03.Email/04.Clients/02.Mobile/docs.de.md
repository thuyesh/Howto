---
title: 'Email: Mobile Clients'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - Mobile
        - Smartphone
page-toc:
    active: false
---

# Email-Clients für Smartphones

How-To für die Einrichtung eines Email-Clients auf Deinem Smartphone:

## Android
- [K9](k9)
- [FairEmail](fairemail)

## SailfishOS
- [Mail App](sailfishos)

## iOS
- [Mail App](ios)
