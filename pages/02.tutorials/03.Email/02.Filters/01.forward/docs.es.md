---
title: "Correo: Reenvío desde Disroot a otra cuenta de correo"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - reenvío
        - configuraciones
page-toc:
    active: false
---

# Reenviando correo de Disroot a otra cuenta

Entendemos que no todos quieran utilizar el correo de **Disroot** como su cuenta de todos los días. La gente tiene sus cuentas de correos desde hace años y es difícil, poco o nada práctico y, en algunos casos, incluso imposible, cambiar a otro proveedor como **Disroot**.

Sin embargo, hay información importante que deberías tener presente.

 - Recepción de notificaciones de los **Foros** y **Nextcloud**. Esos servicios están ligados al correo de **Disroot** y no puedes cambiarlo.
 - Estar al día respecto a lo que sucede en **Disroot**. Cada tres a seis semanas, enviamos un correo a tod@s l@s usuari@s informándoles acerca de desarrollos recientes o en curso, nuevas características y servicios. También enviamos información sobre interrupciones programadas de servicios, si su duración fuera mayor a 20 minutos. Nuestra intención no es generar spam con demasiada información, así que no hay por qué preocuparse.

Esta breve guía, te mostrará cómo reenviar tus correos de **Disroot** a tu dirección de correo preferida (no debería tomarte más de 3 minutos hacerlo).

## Pasos necesarios para reenviar tu correo

1. **Inicia sesión en el webmail** (https://mail.disroot.org)


![](en/login.jpg)


2. **Ve a Configuraciones** (click en el ícono del 'engranaje', abajo, a la izquierda de la ventana)


![](en/webmail1.jpg)


3. En Configuraciones haz click en la pestaña **Filtros**.<br>
Los filtros te ayudan a gestionar tus correos. Dependiendo de tus condiciones de filtrado, puedes mover, copiar o reenviar cualquier correo automáticamente. Esto es bastante directo y sencillo así que si quieres configurar algunos filtros extra simplemente investiga. Aquí veremos como configurar un filtro de reenvío para todos tus correos.


![](en/settings1.jpg)


4. Click en el ícono de **Agregar nuevo filtro**.<br>
Se presentará un cuadro de diálogo que te guiará a través del proceso.


![](en/filters1.jpg)

5. **Completa las reglas de tu filtro**


![](en/filters2.jpg)


 - Dale a tu filtro un nombre
 - Si no hay condiciones especificadas, el filtro se aplicará a todos los correos entrantes, que es lo que queremos en este caso, así que **no agregues ninguna condición** para este filtro.
 - En el menú desplegable **Acciones**, selecciona la opción **Reenviar A**, y agrega la dirección de correo a la que quieres que se reenvíen todos los correos.
 - Una vez que estés listo, haz click en el botón **Hecho**.
 - Para que el filtro quede activado, necesitas **Guardarlo**.


![](en/filters3.jpg)

### Voila!

Desde ahora en adelante, todos los correos dirigidos a tu cuenta de **Disroot** serán reenviados a tu cuenta de correo preferida. Si alguna vez decides realmente cambiar al correo de **Disroot** como tu cuenta principal, simplemente quita la regla o modifícala a tu gusto.
