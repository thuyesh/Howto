---
title: Disroot Webmail
published: true
visible: true
indexed: true
updated:
        last_modified: "April 2020"
        app: Roundcube Webmail
        app_version: 1.4.2
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Disroot Webmail

A webmail, or web-based email, is an email service that can be accessed using the web browser. This means you don't need to install an email client software on your device. The main advantage of webmail over the use of a desktop email client is the ability to send and receive email anywhere from a web browser.

**Disroot** webmail solution is powered by **Roundcube**.


![](logo.png)

# What is Roundcube?
Roundcube is a free and open source webmail software that provides all the functionalities you would expect from an email client: full support for MIME and HTML messages, multiple sender identities, address book with groups and LDAP connectors, threaded message listing, spell checking, support for access control lists (ACL), import/export functions and support for PGP encryption among many other features.

Through the following tutorial, we will learn how to use **Roundcube** and get to know its features.

----

# Table of Contents

## [01. Getting started](Roundcube/01.getting_started)
  - Overview
  - Tasks and basic operations

## [02. Settings](Roundcube/02.settings)
  - [01. Preferences](Roundcube/02.settings/01.preferences)
    - User interface
    - Mailbox view
    - Displaying messages
    - Composing messages
    - Contacts
    - Special folders
    - Server settings
    - Deleting old messages
    - Message highlights
  - [02. Folders](Roundcube/02.settings/02.folders)
  - [03. Identities](Roundcube/02.settings/03.identities)
    - Default identity
    - Add other identities / aliases
    - Sending an email with another identity
  - [04. Responses](Roundcube/02.settings/04.responses)
  - [05. Filters](Roundcube/02.settings/05.filters)
  - [06. Account details](Roundcube/02.settings/06.account_details)
  - [07. Two Factor Authentication](Roundcube/02.settings/07.2fa)
    - Activate Two Factor Authentication
    - Log in when Two Factor Authentication is activated

## [03. Email](Roundcube/03.email)
  - Composing an email

## [04. Contacts](Roundcube/04.contacts)
  - Contacts lists
  - Address book
  - Groups
