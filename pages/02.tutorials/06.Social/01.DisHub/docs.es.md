---
title: Hubzilla
published: true
taxonomy:
    category:
        - docs
page-toc:
     active: false
---
|![](en/round_logo.png)|
|:--:|
|Manual de **DisHub:<br>La instancia Hubzilla de Disroot**|

[**Hubzilla**](https://project.hubzilla.org/page/hubzilla/hubzilla-project) es un poderoso software libre y de código abierto para crear plataformas web descentralizadas y federadas que incluye herramientas para páginas web, wikis, foros, almacenamiento en la nube, redes sociales y más. Cualquier intento de definir a Hubzilla con precisión puede terminar siendo inadecuado.<br>
Mientras que comparte similitudes con otros programas de servicios web, **Hubzilla** ofrece algunas características muy particulares que otros no, como un preciso y completo sistema de permisos que te permite tener el control de la privacidad de la información y el contenido que podrías compartir; identidad nómada (que significa que eres dueño de tu identidad en línea y puedes llevarla/migrarla a través de la red, sin importar los hubs, instancias, servidores); múltiples perfiles y canales; clonado de cuenta; entre otras.

En **Disroot**, pensamos que una pieza de software impresionante y que todos deberían probar por su potencial y el poder que está otorgando a la descentralización y federación de servicios. Así que construimos **DisHub**, la **instancia Hubzilla de Disroot**. Pero, debido a la cantidad de funcionalidades, aplicaciones, servicios y configuraciones de control de usuario, y porque Hubzilla está más allá de la comparación, puede ser algo difícil hasta que lo comprendas y manejes completamente. Por eso hicimos este manual.

Piensa en Hubzilla como tu nueva casa digital: tienes un montón de habitaciones que puedes arreglar y decorar como quieras, es tu lugar para conversar y compartir lo que quieras con las personas que quieras. Y más que eso, porque incluso puedes llevarte tu casa donde quieras (otras instancias de Hubzilla).

Así que, ejercita tu libertad, tu imaginación y se bienvenido a DisHub.

*Solo podemos mostrarte la puerta. Tú eres el que tiene que cruzar a través de ella...*
