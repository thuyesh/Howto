---
title: Hubzilla: Posting and publishing
updated:
published: true
indexed: true
visible: true
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
page-toc:
     active: false
---

# Posting and Publishing

In this section we will see the different options you have when posting or commenting.  
It's recommended to understand the different [permission roles](../permissions) first, so the posts are delivered to those who are meant to.
