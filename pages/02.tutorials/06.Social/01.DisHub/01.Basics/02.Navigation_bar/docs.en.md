---
title: Hubzilla Basics: Navigation Bar
updated:
published: true
visible: true
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
page-toc:
     active: true
---

# Navigation bar

Here is the navigation bar:
![Basics_navigation_ bar](en/Basics_navigation_bar.png)

Here are some of the important icons you need to understand:
1. Profile: ![Profile](en/Basics_profile_icon.png) {.inline}
2. Home Channel: ![home](en/Icon_home.png) {.inline}
3. Network stream: ![home](en/Icon_network_stream.png) {.inline}
4. Public stream: ![home](en/Icon_public_stream.png) {.inline}
5. Connections: ![home](en/Icon_connections.png) {.inline}
6. Directory: ![home](en/Icon_directory.png) {.inline}

!! ![](/home/icons/note.png)
!!Note: On mobile pinned apps are hidden and only accessible through the hamburger menu on the right: ![MobileNavigationBar](en/MobileNavigationBar.png)

![Profile](en/Basics_profile_icon.png) **Profile**: By clicking on it, you access to view and edit your profile, your channel manager, the general settings and to log out.

## 1. Profile: ![Profile](en/Basics_profile_icon.png)
A profile is a collection of information about yourself so you can have different profiles depending on the data you want to share with different users or groups. For example: your public or default profile (which is always available to the general public and can’t be hidden) is GNU/Linux and FOSS related. You set the permissions and establish connections with those who may share those interests. But if you have your group of friends that don’t care about that much, and you don’t want to bore them with it, you can create a new profile with a new set of permissions called “friends”, that allow its members access only to another kind of information.

When you click on the profile icon, you have access to different options.

### View Profiles
Clicking on “View profile” you’ll see your public profile with the information you has submited, such as your name and tags. You can edit this public profile by clicking on the Edit option at the right of the profile title bar.
(See Profiles Settings for more information about configuration)
![basics_profile](en/Basics_profile.png)
[To know more about profiles and the available options...]((../../02.Channels/02.Profiles))

### Edit Profiles
Here you can see your default and created profiles if there any or create new ones.
(See Profiles Settings for more information about creating and managing profiles)
#### [To know more about profiles and the available options...]((../../02.Channels/02.Profiles))

### Channel Manager
From here you can manage your channels and create new ones.<br>
**Note: Channels are different from profiles.**<br>
**A profile** is a collection of information about yourself so you can have different profiles depending on the data you want to share with different users or groups. For example: your public or default profile (which is always available to the general public and can’t be hidden) is GNU/Linux and FOSS related. You set the permissions and establish connections with those who may share those interests. But if you have your group of friends that don’t care about that much, and you don’t want to bore them with it, you can create a new profile with a new set of permissions called “friends”, that allow its members access only to another kind of information.<br>
**A channel**, on the other hand, it’s the space on the web that contains collections of content stored in one place, that’s the stream (will see that later). What kind of content? Well, that’s exactly the point of it. You can create different channels for different kinds of content and specifies whose of your contacts or public can see it. In other words, a profile is information about you and a channel is the space where specific information it’s gathered and showed up.

Click on the **Channel Manager** to access its options.

![basics_channel_manager](en/Basics_channel_manager.png)

From here you can create and switch between channels. The process to create a channel it’s the same as when you create your first one.

#### [To know more about channels and the available options...](../../02.Channels)

### Settings
By clicking on it, you will access to the Account, Channel, Features settings and other configurations.

![basics_settings](en/Basics_settings.png)

!! The settings options are on the left sidebar. So if you want to show the options on mobile you have to clikc on the arrow icon in the nav bar. ![ArrowIcon](en/ArrowIcon.png)

##### Account settings
In the **Account settings** you can change your email address, your password and set Your **technical skill level**. Every level sets different user features according to what you expect or want to do. By default, DisHub skill level is **4. Expert**, in orden to provide most of the features available.

There are five levels. From level 1 on, each one of them progressively enables Additional features.
0. Beginner/Basic
1. Novice - not skilled but willing to learn
2. Intermediate - somewhat comfortable
3. Advanced - very comfortable
4. Expert - I can write computer code
5. Wizard - I probably know more than you do

##### Channel settings:
This is one of the most important settings. These howtos will refer to it a lot.
![basics_channel_settings](en/Basics_channel_settings.png)

## 2. ![home](en/Icon_home.png) {.inline}  Home Channel:
It works like the “wall” of most social networks, it’s the place where you see your posts, the comments on them (if they’re enable), the posts you’ve re-shared, and those from the people you’ve give permissions to write on it.
Everything is already pretty well explained [here](../01.Basics)

## 3. ![home](en/Icon_network_stream.png) {.inline} Network stream:
It looks a lot like the **Home Channel**. Except that here, you see not only your posts and comments, but also the posts and comments of your connections, if you allowed them to.

![Network_stream](en/Network_stream.png)

You can filter those posts in different ways.
1. You can use the Affinity tool bar  [More info about it](../../07.Features/01.Connection_filtering)
2. You can click on the different tabs.
![Filter_tabs](en/Filter_tabs.png)

## 4. ![home](en/Icon_public_stream.png) {.inline} Public stream:
Here, you can see ANYONE posts and comments published in the fediverse, so any Hubzilla, Mastodon, Diaspora, etc., servers.

![Public_stream](en/Public_stream.png)

You can filter those posts by clicking on the cloudwords in **Trending**.

You can also use the **search box** to look for a tag, an user, etc. ![Search_box](en/Search_box.png)

## 5. ![home](en/Icon_connections.png) {.inline} Connections:
It allows to see your connections and to edit them [Informations here](../../03.Connections)

## 6. ![home](en/Icon_directory.png) {.inline} Directory: 
It allows to see and search any channel on hubzilla (not just Dishub). To search a specific channel use the search engine on the upper left side of the directory page

![Directory](en/Directory.png)
