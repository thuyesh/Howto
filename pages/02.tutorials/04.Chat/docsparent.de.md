---
title: Chat
subtitle: Einstellungen, Clients.
icon:  fa-comments
published: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

## Was ist XMPP?

![](de/xmpp_logo.png)

XMPP (Extensible Messaging and Presence Protocol) ist: "*eine Sammlung offenen Technologien Instant Messaging, Präsenz, Mehrpersonen-Chat, Sprach- und Video-ANrufe, Zusammenarbeit, schlanke Middleware, inhaltlicher Zusammenschluss und generalisiertes Routing von XML-Daten.*"

XMPP stellt eine offene und dezentralisierte Alternative zu geschlossenen Instant Messaging-Angeboten dar. DAs XMPP-Protokoll bietet eine Menge Vorteile:

* Es ist **offen** (was bedeutet, dass es frei, offen, öffentlich und leicht verständlich ist)
* Es ist ein **Standard** (anerkannt dur die [IETF](http://www.ietf.org/))
* Es ist **dezentralisiert** (seine Netzwerk-Architektur gleicht der Email-Systematik, was bedeutet, dass jeder seinen eigenen XMPP-Server hosten kann, so dass Individuen und Organisationen die Kontrolle über ihre Kommunikation gewinnen können)
* Es ist **sicher** (SASL und TSL wurden in den XMPP-Kern eingebaut und E2E-Verschlüsselung kann bei Bedarf implementiert werden)
* Es ist **erweiterbar** (Jeder kann eigene Funktionalitäten auf die Kern-Protokolle aufsetzen. Gebräuchliche Erweiterungen werden in den XEP-Serien veröffentlicht

... neben anderen Funktionen.
