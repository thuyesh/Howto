---
title: SailfishOS
published: true
visible: false
taxonomy:
    category:
        - docs
---

1. **Ouvrez l'application Paramètres** et faites défiler jusqu' à "**Comptes**".

2. Ajout d'un nouveau compte **XMPP**..

3. **Mise en place du compte** Remplissez les détails
 - **Nom d'utilisateur:** *Votre nom_d_utilisateur_Disroot*@disroot.org (vous pouvez également spécifier le nom d'utilisateur et le domaine séparément)
 - **Mot de passe:** *Mot de passe Disroot*
 - **Adresse du serveur:** (si vous avez spécifié seulement votre nom d'utilisateur Disroot, ici vous pouvez ajouter le domaine) *disroot.org*
 - **Port:** 5222

![](en/sailfish_xmpp1.png)

4. **Glisser accepter, et vous avez fini!**
Maintenant, vous êtes prêt à convertir les gens de quitter les applications malveillantes des entreprises et de sauter sur le vaisseau de la fédération.

SailshishOS intègre tous les messages dans une seule application pour que vous puissiez maintenant envoyer des SMS, utiliser skype et chatter d'un seul endroit.

**Tous vos contacts sont synchronisés entre tous les clients, de sorte que vous pouvez utiliser le chat Disroot sur tous les périphériques en même temps.**
