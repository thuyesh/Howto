---
title: 'Mumble'
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Mumble
        - Plumble
        - audioconferencia
        - audio
page-toc:
  active: false
---

![](/home/icons/mumble.png)

**Mumble** es un software de chat de voz, de baja latencia y alta calidad, de código abierto, pensado en principio para utilizar mientras se juega, pero que puedes utilizar para hacer audioconferencias y chatear.
<br>No es necesaria una cuenta para utilizarlo.

## Clientes
Puedes usar Mumble desde tu escritorio o teléfono.

### [Escritorio: Mumble](mumble)
- Accede a los canales desde tu escritorio.

### [Android: Plumble](plumble)
- Accede a los canales desde tu dispositivo con Android.

### [iOS: Mumble](mumbleios)
- Accede a los canales desde tu dispositivo con iOS.
