---
title: 'Mumble'
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Mumble
        - Plumble
        - audioconference
        - audio
page-toc:
  active: false
---

![](/home/icons/mumble.png)

**Mumble** est un logiciel de chat vocal open-source, à faible latence et de haute qualité, principalement destiné à être utilisé pendant les jeux, mais que vous pouvez utiliser pour toute audioconférence/chat.
<br>Aucun compte d'utilisateur n'est nécessaire pour l'utiliser.

## Plusieurs clients
Vous pouvez utiliser Mumble à partir de votre bureau ou de votre téléphone.

### [Bureau : Mumble](mumble)
- Accédez aux chaînes depuis votre bureau.

### [Android : Plumble](plumble)
- Accédez aux chaînes depuis votre appareil Android.

### [iOS : Mumble](mumbleios)
- Accédez aux chaînes à partir de votre appareil iOS.
