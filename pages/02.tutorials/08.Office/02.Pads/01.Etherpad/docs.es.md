---
title: 'Pads: Etherpad'
published: true
visible: false
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

|![](/start/icons/etherpad.png)|
|:--:|
|**Etherpad** y **EtherCalc** son aplicaciones colaborativas para edición multiusuario en tiempo real de documentos. Puedes acceder a ellas directamente desde tu navegador en: [https://pad.disroot.org](https://pad.disroot.org) y [https://calc.disroot.org](https://calc.disroot.org). No es necesario tener una cuenta de usuario para utilizarlos. Sin embargo, nuestra nube viene con un plugin muy útil que te ayuda a mantener un registro de todos tus pads, como si fueran uno de tus archivos.|
