---
title: 'Pads (Blocs de notas)'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

|![](/start/icons/etherpad.png)|
|:--:|
|**Etherpad** y **EtherCalc** son aplicaciones colaborativas para edición multiusuario en tiempo real de documentos. Puedes acceder a ellas directamente desde tu navegador en: [https://pad.disroot.org](https://pad.disroot.org) y [https://calc.disroot.org](https://calc.disroot.org). No es necesario tener una cuenta de usuario para utilizarlos. Sin embargo, nuestra nube viene con un plugin muy útil que te ayuda a mantener un registro de todos tus pads, como si fueran uno de tus archivos.|

|![](en/note.png) **AVISO**|
|:--:|
|"Pads" y "calcs (hojas de cálculo)" no son archivos que contienen tus datos sino que son links a tu trabajo almacenado en https://pad.disroot.org o en https://calc.disroot.org.|

# La idea detrás de los pads...
... es muy simple. Es un editor de texto/planilla de cálculo que "vive" en la web. Todo lo que tipeas es escrito en tu bloc automáticamente. Puedes trabajar sobre un documento con muchas personas al mismo tiempo, sin necesidad de salvar y pasarse copias de los documentos unos a otros. Una vez que tu trabajo está hecho, puedes exportar el pad al formato que elijas.

|[**Ownpad**](pad_disroot)|[**Etherpad**](etherpad)|[**EtherCalc**](ethercalc)|
|:--:|:--:|:--:|
|Plugin para la nube de Disroot|Editor web colaborativo de documentos de texto|Planillas de cálculo colaborativas|
