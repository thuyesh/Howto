---
title: 'Pads'
published: true
visible: true
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

O Etherpad e o Ethercalc são duas aplicações de edição colaborativa online entre vários utilizadores de documentos.  Pode aceder a estas duas ferramentas diretamente a partir do seu browser nos endereços: https://pad.disroot.org e https://calc.disroot.org Não é necessário conta de utilizador no Disroot para os utilizar. No entanto a nossa cloud tem um plugin de "Pads" ("Pad" e "Calc") que o ajuda a manter um registo dos seus pads e a organizá-los. Como se fosse um dos seus ficheiros na sua conta de cloud.

**Nota!** "Pads" and "calcs" não são os ficheiros que contêm o conteúdo dos Pads, mas link para onde o seu trabalho no Pad está armazanado em https://pad.disroot.org ou https://calc.disroot.org.

# A ideia por detrás de um Pad
A ideia por detrás deste software é bastante simples. É um texto ou folha de cálculo que "vive" na net. Tudo o que escreve é gravado no pad automaticamente. Pode trabalhar num documento com várias pessoas ao mesmo tempo, sem ser necessário gravar e enviar cópias uns aos outros. Quando o trabalho estiver terminado, pode exportar o pad para diferentes formatos à sua escolha. Para ler mais acerca do tutorial sobre como trabalhar com um pads veja esta página (página em construção).

# Utilização
Em baixo pode ler sobre como utilizar o ownpad - o plugin para a cloud do Disroot.

# Criar um novo Pad
Criar um novo pad é da mesma maneira que para criar um novo ficheiro. Caregue no ícone **"+"** e escolha criar ou um pad (editor de texto) or calc (folha de cálculo). Assim que der um nome, será criado um novo ficheiro com a extensão .pad ou .calc. Pode mover, partilhar este ficheiro como se fosse qualquer outro ficheiro na sua conta de cloud.

![](en/pads_add.gif)

# Abrir o novo pad
Para abrir o pad que acabou de criar basta carregar no ficheiro. A aplicação irá abrir o pad que foi criado utilizando ou pad.disroot.org ou calc.disroot.org a partir do interface da cloud. Pode agora trabalhar no seu documento e quando tiver terminado, carregue no no botão vermelho no canto superior direito para fechar a janela. Todas as alterações são submetidas a partir do momento em que começa a escrever, por isso não é necessario estar sempre a 'salvar' o ficheiro. Como o ficheiro está na net pode regressar a este ficheiro sempre que quiser.

![](en/pads_inapp_name.gif)

# Partilhar o pad

Partilhar pads com outros utilizadores do Disroot é igual a partilhar um ficheiro normal. Carregue na opção "partilhar" e escolha com quem quer partilhar o ficheiro. **Nota:** usar o link de partilha pública para partilhar este ficheiro que está na sua conta de cloud não irá funcionar fácilmente pois stes não são ficheiros normais (como por exemplo, um ficheiro .txt, .odt, .doc).

![](en/pads_inapp_name2.gif)

De modo a poder partilhar o pad com alguém que não é utilizador do Disroot, deve partilhar o link de URL público do pad. Para fazer isto, abra o pad no browser e passando com a seta do rato no nome do pad. Verá o link URL do pad e depois pode copiá-lo e enviá-lo a qualquer pessoa com quem queira partilhar o pad.

# Apagar um pad
É impossível apagar um pad. Uma vez criados, ficam online. De fato qualquer pessoa que consiga adivinhar o endereço de URL público do pad pode aceder a ele. É por isso que o plugin de pads que a cloud do Disroot usa, o ownpad cria os pads usando caracteres aleatórios no no URL em vez de o nome que utilizou quando criou o ficheiro '.pad' ou '.calc' na sua conta de cloud. Deste modo os links URL públicos são impossíveis de adivinhar tornando o pad mais seguro de utilizar e mais difícil de aceder a outras pessoas que não você e as outras pessoas com quem decidiu partilhar o pad.
