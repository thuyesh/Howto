---
title: Office
subtitle: "Pads, Pastebin, Mumble, Polls & File Sharing"
icon: fa-file-text
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - office
        - poll
        - pastebin
        - file
        - sharing
        - disapp
page-toc:
    active: false
---

# Office Tools

Disroot provide a bunch of useful web tools you may want to give them a try.

---

For extra convenience, there is also a **Disroot** app that gathers all this tools and other **Disroot** services:

### [DisApp](../user/disapp)
