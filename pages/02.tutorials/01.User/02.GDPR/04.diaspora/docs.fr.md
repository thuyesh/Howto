---
title: "Diaspora*: Faire la demande pour vos données de Profil / Photos"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Diaspora*
    app_version: 0.7
taxonomy:
    category:
        - docs
    tags:
        - user
        - diaspora
        - gdpr
visible: true
page-toc:
    active: false
---

Obtenir vos données de profil fournies à **Diaspora** est entièrement automatisé et peut être fait n'importe quand. Tout ce que vous ayez à faire est :

1. Connectez-vous à diaspora [https://pod.disroot.org](https://pod.disroot.org)

2. Aller dans vos paramètres de compte:

![](fr/settings.gif)

3. Une fois dans les paramètres, allez tout en bas et choisissez quelles données vous voulez récupérer, les données de profil, ou les photos que vous avez uploadées.

![](fr/request.gif)

Une fois que l'un de ces boutons a été cliqué, vous devez attendre quelques instants pour que la requête soit traitée (le temps dépend de la taille de votre compte.

![](fr/wait-request.png)

4. Une fois les données prêtes au téléchargement, vous pouvez les obtenir en cliquant sur le lien.

![](fr/data-download.png)
