---
title: "Nextcloud: Esportare i contatti"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

I contatti possono essere esportati in qualsiasi momento in modo molto semplice.

1. Accedi al [cloud](https://cloud.disroot.org)

2. Seleziona l'applicazione "*Contatti*".

![](en/select_app.gif)

3. Seleziona l'opzione **Impostazioni** in basso nella barra laterale.

4. Seleziona i "*tre puntini*" a fianco della rubrica che vuoi esportare.

5. Seleziona "*Scarica*" per esportare i contatti. I tuoi contatti saranno salvati nel formato .vcf.

![](en/export_data.gif)
