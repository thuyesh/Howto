---
title: 'Nextcloud: Exportar Marcadores'
published: true
indexed: true
updated:
    last_modified: "Julio 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - usuario
        - nube
        - marcadores
        - rgdp
visible: true
page-toc:
    active: false
---

Exportar la información almacenada en la nube de tus marcadores es muy sencillo con **Disroot**.

1. Inicia sesión en la [nube](https://cloud.disroot.org)

2. Selecciona la aplicación Marcadores

![](es/select_app.gif)

3. Selecciona Configuración (abajo, en la barra lateral izquierda) y presiona el botón **"Export"**

![](es/export.gif)
