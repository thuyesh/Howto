---
title: GDPR: Nextcloud
published: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - user
        - gdpr
visible: true
page-toc:
    active: false
---
## How-to export your...

  - [**Bookmarks**](bookmarks)
  - [**Calendars**](calendar)
  - [**Contacts**](contacts)
  - [**Files and Notes**](files)
  - [**News**](news)
