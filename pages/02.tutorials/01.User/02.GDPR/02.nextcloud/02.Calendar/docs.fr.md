---
title: Nextcloud: Exporter ses Calendriers/Agendas
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

Exporter vos calendriers/agendas est très simple:

1. Connectez-vous sur [cloud](https://cloud.disroot.org)

2. Choisissez l'application Agenda

![](fr/select_app.gif)

3. Exportez n'importe lequel de vos agendas / calendriers ou de ceux auquels vous êtes abonnés.
Sélectionnez le bouton de menu *"trois petits points"* à côté du calendrier que vous voulez exporter et cliquez sur  *"Télécharger"*. Le calendrier exporté est sauvé au format .ics .

![](fr/export-calendar.gif)

Répétez cette procédure pour tous les autres calendriers / agendas que vous voulez exporter.
