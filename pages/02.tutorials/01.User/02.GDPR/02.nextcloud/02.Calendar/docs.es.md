---
title: "Nextcloud: Exportar Calendarios"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - usuario
        - nube
        - calendario
        - rgdp
visible: true
page-toc:
    active: false
---

Exportar calendarios es muy simple:

1. Inicia sesión en la [nube](https://cloud.disroot.org)

2. Selecciona la aplicación Calendario

![](es/select_app.gif)

3. Para exportar cualquiera de los calendarios que tengas, haz click en el menú de *"tres puntos"* que estará al lado del que quieras respaldar y selecciona la opción *"Descargar"*. El calendario exportado se guarda como un archivo .ics.

![](es/export-calendar.gif)

El proceso es el mismo para cualquier calendario que quieras exportar.
