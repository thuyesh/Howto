---
title: RGPD: Nextcloud
published: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - user
        - gdpr
visible: true
page-toc:
    active: false
---
## Comment exporter vos ...

  - [**Fichiers et Notes**](files)
  - [**Contacts**](contacts)
  - [**Calendriers**](calendar)
  - [**News**](news)
  - [**Favoris**](bookmarks)
