---
title: "Discourse: Exporter vos posts du Forum"
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Discourse
    app_version: 2.3
taxonomy:
    category:
        - docs
    tags:
        - user
        - forum
        - discourse
        - gdpr
visible: true
page-toc:
    active: false
---

**Discourse**, le logiciel utilisé par **Disroot** pour son **Forum**, vous permet d'exporter le contenu textuel de tous vos posts vers un fichier .csv (qui est supporté par la plupart des logiciels de tableurs, Libreoffice, Openoffice, Gnumeric, Excel).

**Pour exporter vos posts à partir de Discourse:**

- Cliquer sur la photo de votre avatar en haut à droite.

- Cliquer sur votre nom

- CLiquer sur *"Télécharger mes Posts"*

- Confirmer en cliquant sur *Oui*

**NOTE:** Comme vous le voyez ci-dessous dans le fichier video, vos données ne peuvent être téléhargées qu'une seule fois par 24h.

![](fr/export_data_discourse_01.gif)

Vous recevrez un message système vous avertissant que les données sont prêtes à être téléchargées et vous indiquant un lien de téléchargement.
Si vous avez activé les notifications par email, vous recevrez cette information par email, ainsi que le lien de téléchargement.
