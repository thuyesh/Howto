---
title: "XMPP Chat"
published: true
indexed: true
updated:
    last_modified: "June 2020"		
    app: Converse
    app_version:
taxonomy:
    category:
        - docs
    tags:
        - user
        - chat
        - gdpr
visible: true
page-toc:
    active: false
---
