---
title: Come applicare l'Alias di posta elettronica
published: true
visible: true
taxonomy:
    category:
        - docs

---

Gli alias sono disponibili ai contributori regolari. Con "collaboratori regolari", ci riferiamo a coloro che ci "donano" almeno una tazza di caffè al mese.

Non stiamo promuovendo il caffè, che è in realtà un esempio a portata di mano su ciò che è [sfruttamento e disuguaglianza](http://thesourcefilm.com/) (http://www.foodispower.org/coffee/). E abbiamo pensato che è un buon modo per permettere alle persone di misurare se stessi quanto possono dare.
Si prega di prendere tempo per esaminare il vostro contributo. Se è possibile 'acquistare' noi una tazza di Rio De Janeiro caffè un mese che è OK, ma se potete permettervi un doppio decaffeinato Frappuccino con  panna extra di soia al mese, quindi ci può davvero aiutare a mantenere la piattaforma Disroot in esecuzione e assicurarsi che sia disponibile gratuitamente per altre persone con meno mezzi.

Abbiamo trovato questa [lista](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) di prezzi per tazze di caffè del mondo, potrebbe non essere molto accurato, ma dà una buona indicazione dei costi diversi.

Per richiedere alias è necessario per completare questo  [modulo](https://disroot.org/forms/alias-request-form).
