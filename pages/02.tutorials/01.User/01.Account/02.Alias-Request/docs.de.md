---
title: Email-Alias beantragen
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - alias
page-toc:
    active: false
---

# Email-Alias beantragen

Aliase sind verfügbar für regelmäßige Unterstützer. Mit regelmäßigen Unterstützern meinen wir die Community-Mitglieder, die uns mindestens eine Tasse Kaffee im Monat "kaufen".

Es ist jetzt nicht so, dass wir hier Kaffee anpreisen wollen (tatsächlich ist Kaffee jedoch eine sehr griffige Analogie für [Ausbeutung und Ungleichheit](http://www.foodispower.org/coffee/)). Wir denken, das ist eine gute Möglichkeit, jeden selbst bestimmen zu lassen, wie viel er geben kann.

Nimm Dir ruhig Zeit, die Höhe Deines Beitrags abzuwägen.

Wenn Du uns einen **Rio de Janeiro**-Kaffee im Monat "kaufen" kannst, ist das OK. Wenn Du Dir jedoch einen *Doppelten entkoffeinierten Soja Frappuccino mit einem extra Shot und Sahne* im Monat leisten kannst, dann hilfst Du uns wirklich, die **Disroot**-Plattform am Laufen zu halten und sicherzugehen, dass sie frei zugänglich bleibt für andere Menschen mit weniger Mitteln.

Wir haben diese [Liste](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) gefunden mit Kaffeepreisen rund um die Welt. Die Liste ist vielleicht nicht hundertprozentig korrekt oder gar tagesaktuell, aber Du hast in ihr einen guten Indikator für die verschiedenen Preise.

Um ein Alias zu beantragen, fülle bitte dieses [Formular](https://disroot.org/forms/alias-request-form) aus.
