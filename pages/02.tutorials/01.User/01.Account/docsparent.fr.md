---
title: Gestion du Compte
published: true
indexed: true
visible: true
updated:
    last_modified: "July 2019"		
taxonomy:
    category:
        - docs
    tags:
        - user
        - administration
page-toc:
    active: false
---

# Gestion du Compte

#### [Gérer votre compte Disroot](ussc/)

#### [Demander un alias email](alias-request)
