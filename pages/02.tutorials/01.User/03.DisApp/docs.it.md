---
title: DisApp
published: true
visible: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: DisApp
    app_version: 1.2
taxonomy:
    category:
        - docs
    tags:
        - user
        - disapp
page-toc:
    active: false
---

# L'applicazione della comunità Disroot
![](/home/icons/disapp.png)
**Un'applicazione per accedere a tutte le altre**

Il disrooter **Massimiliano** ha visto il potenziale di un'app **Disroot** e ha deciso di affrontare questa sfida con un approccio inaspettato. Ha sviluppato l'app **Disroot "coltellino svizzero"** che aiuta e guida i disrooter verso le app consigliate e i consigli su come impostare il tutto. L'app sceglierà la migliore (a nostro avviso) app per e-mail, chat, ecc. Per quei servizi che invece non hanno un'app dedicata li aprirà nel browser. L'app fornisce anche indicazioni per tutti i tutorial della community che abbiamo raccolto nel corso degli anni per aiutare le persone a utilizzare i servizi forniti.

L'app è accessibile attraverslo l'unico Android store libero e fidato: [f-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/).
