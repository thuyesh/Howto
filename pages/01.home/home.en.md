---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Welcome to the Disroot's How-to site.

The main goal of this site is to help you find your way around the various **Disroot** services.

To cover all the services, with all its **Disroot**'s provided features, for all platforms and/or Operating Systems it's a very ambitious and time consuming project that requires a lot of work. And because we think it could be beneficial not only for our users (disrooters) but the entire **Free Software** and **Open Source** communities running the same or similar software, any help from disrooters is always needed and welcome.<br>
So, if you think there's a missing tutorial, the information is not accurate or could be improved, please contact us and (even better) start writing a how-to yourself.<br>

To know the different ways you can contribute, please check this [section](/contribute).

